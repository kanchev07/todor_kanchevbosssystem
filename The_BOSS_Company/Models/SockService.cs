﻿using System;
using System.Timers;


namespace The_BOSS_Company.Models
{
    public sealed class SockService
    {
        private static decimal currentPrice = GetRandomNumber();

        static readonly SockService _instance = new SockService();
        public static SockService Instance
        {
            get
            {
                return _instance;
            }
        }
        SockService()
        {
            Timer myTimer = new System.Timers.Timer();            
            myTimer.Elapsed += new ElapsedEventHandler(myEvent);
            myTimer.AutoReset = true;            
            myTimer.Interval = 3600000;                  
            myTimer.Enabled = true;  
        }
        private void myEvent(object source, ElapsedEventArgs e)
        {
            generatePrice();
        }
    

        private void generatePrice()
        {
            currentPrice *= GetRandomNumber();
        }

        public decimal GetPrice()
        {
            return currentPrice;
        }

        public void UpdatePrice()
        {

        }
        public static decimal GetRandomNumber()
        {
            Random random = new Random();
            return (decimal)random.NextDouble() * (2 - 0) + 0;
        }
    }
}