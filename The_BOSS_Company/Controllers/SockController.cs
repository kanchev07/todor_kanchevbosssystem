﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using The_BOSS_Company.Models;

namespace The_BOSS_Company.Controllers
{
    public class SockController : ApiController
    {
        public decimal Get()
        {
            return SockService.Instance.GetPrice();
        }
    }
}
