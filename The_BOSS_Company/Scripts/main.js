﻿var baseUri = 'http://localhost:59120/';

$(document).ready(function () {
    
    

    
});

function register() {
    var uri = baseUri + 'api/Account/Register';
    var username = $("#username").val();
    var password = $("#password").val();
    var confirmPassword = $("#confirmPassword").val();
    var data = {};
    data.email = username;
    data.password = password;
    data.confirmPassword = confirmPassword;
    $.ajax({
        type: "POST",
        url: uri,
        dataType: 'json',
        async: false,
        data: data,        
        success: function () {
            alert('Thanks for your comment!');
        }
    });
}

function login() {
    var uri = baseUri + 'Token';
    var username = $("#username").val();
    var password = $("#password").val();
    var data = {};
    data.grant_type = 'password';
    data.username = username;
    data.password = password;
    $.ajax({
      type: "POST",
      url: uri,
      dataType: 'json',
      async: false,
      data: data,      
      success: function (data) {          
          sessionStorage.setItem('access_token', data.access_token);
          showUserInfo(username);
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
          alert(errorThrown);
      }
        
  });
    
}

function logoff() {
    var uri = baseUri + 'api/Account/Logout';
    $.ajax({
        type: "POST",
        url: uri,
        async: false,        
        beforeSend: function(request) {
            request.setRequestHeader("Authorization", 'Bearer ' + sessionStorage.getItem('access_token'));
        },
        success: function () {            
            sessionStorage.removeItem('access_token');
            alert('Thanks for your comment!');
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
}

function showUserInfo(username) {
    $('#login_register').hide();
    $('#userInfo').text('Welcome ' + username);
    $.ajax({
        url: baseUri + "api/Sock", success: function (result) {
            $('#price').text('Current sock price is  ' + result);
        }
    });
}